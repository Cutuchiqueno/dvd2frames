#!/bin/bash

# Internal: identifies movie folders with no frames in it.
# 
# The function looks for folders in the current working directory which contain
# a movie folder with a movie file but no frame images in the size of 240p30.
#
# TODO Parametize Folder to look at
#
# Returs a list of directory paths
get_folders(){
    # Collect all normal folder names in the current directory
    for folder in $(find -maxdepth 1 -type d -regex '\./\w.*'); do

        # when there is no folder for 240p30 frames, create one
        # TODO convert to function
        if [ ! "$(find $folder -type d -name '240p30')" ]; then
            mkdir -p "${folder}/frames/240p30"
        # when there is no movie folder, create one
        elif [ ! "$(find $folder -type d -name 'movie')" ]; then
            mkdir -p "${folder}/movie"
        fi

        # when there already exist a 240p30 folder and when there are even
        # images inside the folder, skip movie
        if [ "$(find ${folder}/frames/240p30/ -type f -name '*.png')" ]; then
            continue
        # when there are no frame images in the format 240p30, test if a movie
        # file is available, if not skip movie  
        elif [ ! "$(find ${folder} -type f -name '*.mkv')" ]; then
            continue
        else
            echo "$folder"
        fi
    done
}

# Internal: Extracts frames as images from a movie file
#
# Frames are converted to PNG images with a height of 240 pixels. An image is
# extracted for each quarter of a second throughout the video file.
# 
# Yields a folder with movie length in seconds times four *.png files
#
# $1 - The root path of the movie project sceleton
#
# Returns nothing.
extract_frames(){
    slug=$(basename "$1")
    movie="${slug}.mkv"

    ffmpeg \
        -i "${1}/movie/$movie" \
        -vf 'scale=iw*sar:ih,scale=-1:240,fps=4' \
        "${1}/frames/240p30/${slug}_%05d.png" \
        # -vf 'fps=1' = 1 pro Sekunde; `fps=1` 2 pro Sekunde; `fps=1/60` alle 60 Sekunden 1 Bild
        # %05d Namingpattern für die Ausgabe Dateien
}

# Internal: Gets main title of all tracks on a DVD.
# 
# Takes a sequence of titles and looks which one is the longest title.
# If none exists that is longer than 1h it looks for 3-4 titles with a length
# between 20m and 59m and decides that it is a series.
# at the end the user has to confirm or input the number of the title which
# should be backuped.
#
# $1 - DVD metadata as provided by the lsdvd command.
#
# Returns the number of the DVD track as an integer.
maintitle(){
    feature=$(echo "$1" | awk '$1~/Longest/ { print $3}')
    # TODO Könnte noch prüfen, ob es ein Feature gibt, oder ob es Serientitel sind
    echo "$feature"
}

# confirm selected feature or select another title from the title list
#
# $1: selected title
# $2: title list
# confirmation(){
#     echo "${2}\n"
#     echo "DVDDigitizer identified title $1 to be the main title!"
#     read -p "Is this correct? ([Y], [n]o, select title number: " $selection
# }

# Internal: Extracts a video track from a DVD
#
# The resulting video file is H.264 encoded and, if available, is bundled
# together with German and English audio tracks and subtitles in a Matroska
# Container. The resulting video is scaled to 720p30.
#
# TODO: Schleife für mehrere Titel noch nicht implementiert
# Backup DVD (funktioniert aus WSL nicht)
#
# Yields a movie file in a folder movie of the movie project sceleton.
#
# $1 - Number of video titles that should be ripped from the DVD.
# $2 - Movie slug according to the DVD.
#
# Returns nothing.
videofiles () {
    HandBrakeCLI --input=/dev/sr0 --title="$1" --output="${2}-mkv" \
                 --audio-lang-list="eng,deu" --subtitle-lang-list="eng,deu" \
                 -Z "H.264 MKV 720p30" \
                 --markers
    # --loose-anamorphic (wegen der Bildverzerrung auf der DVD)
}

# Internal: Wrapper function to streamline DVD processing functions.
# 
# Returns nothing.
process_dvd(){
    dvd_info=$(cat ./lsdvdout.txt)
    # dvd_info=$(lsdvd)

    slug=$(echo "$dvd_info" | awk --field-sep ' ' '$1~/Disc/ {print tolower($0)}' | cut -d' ' -f3-)
    mkdir -p "$slug"/{movie,frames/240p30}

    title=$(maintitle dvd_info)

    videofiles "$title" "$slug"
}

# Internal: Wrapper function to streamline video processing functions.
#
# Returns nothing.
process_video(){
    folders=$(get_folders)
    extract_frames "$folders"
}

# Public: Creates frame images from a DVD or a video file.
#
# Looks for a DVD in /dev/sr0 or for folders with video files according to the
# convention SLUG/movie/slug.mkv. When there is a DVD, the DVD is first
# converted into a movie file in a corresponding project sceleton. Afterwards
# for all projects with a movie file but without frame images, frames are
# generated from the movie file and stored into a SLUG/frames/240p30/ folder.
#
# Yields a folder MOVIEFOLDER/frames/240p30/ with movie length in sec times 4
# images following the naming scheme MOVIEFOLDER_{%5d}.png. In case a DVD is
# present and no movie file exists it yields a folder
# MOVIEFOLDER/movie/MOVIENAME.mkv
#
# Returns exit code 0 or 1
dvd2frames(){
    if [ -e /dev/sr0 ]; then
        process_dvd
    fi

    process_video
}

dvd2frames

# TODO Titelnummern für Serien DVDs identifizieren und Rip Prozess anpassen
#       entweder mit den Zeitstempeln von lsdvd berechnen, oder vielleicht geht
#       es auch mit dem --min-duration Parameter von HandBrake
# TODO Documentation