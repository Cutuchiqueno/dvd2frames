`get_folders()`
---------------

Internal: identifies movie folders with no frames in it.

The function looks for folders in the current working directory which contain a movie folder with a movie file but no frame images in the size of 240p30.

TODO Parametize Folder to look at

Returs a list of directory paths


`extract_frames()`
------------------

Internal: Extracts frames as images from a movie file

Frames are converted to PNG images with a height of 240 pixels. An image is extracted for each quarter of a second throughout the video file.

Yields a folder with movie length in seconds times four *.png files

* $1 - The root path of the movie project sceleton

Returns nothing.


`maintitle()`
-------------

Internal: Gets main title of all tracks on a DVD.

Takes a sequence of titles and looks which one is the longest title. If none exists that is longer than 1h it looks for 3-4 titles with a length between 20m and 59m and decides that it is a series. at the end the user has to confirm or input the number of the title which should be backuped.

* $1 - DVD metadata as provided by the lsdvd command.

Returns the number of the DVD track as an integer.


`videofiles()`
--------------

Internal: Extracts a video track from a DVD

The resulting video file is H.264 encoded and, if available, is bundled together with German and English audio tracks and subtitles in a Matroska Container. The resulting video is scaled to 720p30.

TODO: Schleife für mehrere Titel noch nicht implementiert Backup DVD (funktioniert aus WSL nicht)

Yields a movie file in a folder movie of the movie project sceleton.

* $1 - Number of video titles that should be ripped from the DVD.
* $2 - Movie slug according to the DVD.

Returns nothing.


`process_dvd()`
---------------

Internal: Wrapper function to streamline DVD processing functions.

Returns nothing.


`process_video()`
-----------------

Internal: Wrapper function to streamline video processing functions.

Returns nothing.


`dvd2frames()`
--------------

Public: Creates frame images from a DVD or a video file.

Looks for a DVD in /dev/sr0 or for folders with video files according to the convention SLUG/movie/slug.mkv. When there is a DVD, the DVD is first converted into a movie file in a corresponding project sceleton. Afterwards for all projects with a movie file but without frame images, frames are generated from the movie file and stored into a SLUG/frames/240p30/ folder.

Yields a folder MOVIEFOLDER/frames/240p30/ with movie length in sec times 4 images following the naming scheme MOVIEFOLDER_{%5d}.png. In case a DVD is present and no movie file exists it yields a folder MOVIEFOLDER/movie/MOVIENAME.mkv

Returns exit code 0 or 1


